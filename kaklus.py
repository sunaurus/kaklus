import pygame


class Player(pygame.sprite.Sprite): 
   
    # Liikumine
    xchange = 0
    ychange = 0
 
    # Mitu korda saab parajasti hüpata
    jumps = 1
    
    #elud
    hp = 10
    
    #graafika
    scale = 3
    moveleft = False
    moveright = False
    stunned = False
    jumping = False
    kicking = False
    zapping = False
    won = False
    lost = False
    busy = False
    direction = ""
    rectwidth = 39
    rectheight = 82
    scaledwidth = rectwidth * scale
    scaledheight = rectheight * scale
    count = 0

    animframe = 0
    animation = "idle"
    frame_count = 0
    

    
    def __init__(self,x,y,spritesheet,direction): 
        pygame.sprite.Sprite.__init__(self) 
                   
        self.sheet = pygame.transform.scale(pygame.image.load("tegelased/" + spritesheet).convert_alpha(), (1606*self.scale,86*self.scale))
        self.rect = pygame.Rect(x, y, self.scaledwidth, self.scaledheight)
        self.rect.x = x
        self.rect.y = y
        self.spritesheet = spritesheet
        self.lifeframes = []
        self.direction = direction
        self.hpremoved = False
        if x > 200:
            self.lifex = 1000 - 50 - 344
        else:
            self.lifex = 50

        #loome sõnastiku kõikide animatsioonidega
        self.sprites = dict((animation,[]) for animation in ["idle","stun","run","jump","fall","zap","kick","win"])
        self.create_anims([2,43,84,43],[39,39,38,39],[72,71,70,71],"idle",4)
        self.create_anims([179,230,280,330,378,427],[49,48,48,46,47,48],[61,61,61,61,62,62],"run",6)
        self.create_anims([124],[53], [76], "stun", 1)
        self.create_anims([477],[35],[69],"jump",1)
        self.create_anims([514],[50],[80],"fall",1)
        self.create_anims([566,603,663,749,864,749,663,603,566],[34,58,84,113,148,113,84,58,34],[71,69,69,69,69,69,69,69,69,71],"zap",9)
        self.create_anims([1014,1054,1091,1125,1188,1054],[38,35,32,61,63,35],[67,68,74,68,68,68],"kick", 6)
        self.create_anims([1253,1307,1358],[52,49,48],[82,81,82],"win",3)
        
        #elude frameide jaoks list
        
        for i in range(11):
            self.lifeframes.append(pygame.image.load("elud/"+ self.spritesheet[:5] + str(i) + ".png").convert())

        
    def create_anims(self, x, width, height, animation, frames):
        scale = self.scale
        y = 2
        widthsum = 0
        for i in range(frames):
            patch_rect = (x[i]*scale, y*scale, width[i]*scale, height[i]*scale)
            new = pygame.Surface((max(width)*scale,self.scaledheight), pygame.SRCALPHA, 32)
            #väike viga spritesheetis (idle animatsiooni 3. raamis), parandan pigem koodis sest eelmise spritesheeti tegemine võttis pool tundi ja ma ei viitsi uuesti alustada
            if animation == "idle" and i == 2:
                new.blit(self.sheet, (1*scale,(self.scaledheight-height[i]*scale)), patch_rect) 
            else:
                new.blit(self.sheet, (0,(self.scaledheight-height[i]*scale)), patch_rect) 
            self.sprites[animation].append(new)
           
    #joonistame selle raami mida parajasti vaja
    def draw(self, target_surface, other):
        self.update(other)
        
        target_surface.blit(self.lifeframes[self.hp], [self.lifex,50])
       
        if self.animframe > len(self.sprites[self.animation])-1:
            self.animframe = 0
        if self.direction == "right":
            target_surface.blit(self.sprites[self.animation][self.animframe], [self.rect.x,self.rect.y])
        else:
            target_surface.blit(pygame.transform.flip(self.sprites[self.animation][self.animframe], True, False), [self.rect.x+self.scaledwidth-(self.sprites[self.animation][self.animframe].get_width()),self.rect.y])

                
    # uuendame tegelase asukohta ning animatsiooni
    def update(self, other): 
        global backgroundx
        
        
        
        # mängu lõpp
        
        if self.hp < 1: 
            self.animation = "fall"
            self.rect.y += 5
            
        elif other.hp < 1:
            self.animation = "win"
            if self.rect.x > 505-self.scaledwidth/2:
                self.rect.x -= 5
            elif self.rect.x < 500-self.scaledwidth/2:
                self.rect.x += 5
            
        else:
            # akna äärtest ei saa välja minna
            if self.rect.x <= 50:
                if backgroundx > 0:
                    backgroundx -= 4
                    other.rect.x += 4
                self.rect.x = 51

            if self.rect.x >= 950-self.scaledwidth:
                if backgroundx < 300:
                    backgroundx += 4
                    other.rect.x -= 4
                self.rect.x = 949-self.scaledwidth
        
            
            # animatsioonid
            if not self.stunned: 
                if self.kicking:
                    self.animation = "kick"
                    if self.frame_count >= 59:
                        self.kicking = False
                        self.busy = False
                
                elif self.zapping:
                    self.animation = "zap"
                    if self.frame_count >= 89:
                        self.zapping = False
                        self.busy = False
                        
                elif self.moveleft:
                    self.direction = "left"
                    self.xchange = -5
                    if self.rect.y == 550-self.scaledheight:
                        self.animation = "run"

                elif self.moveright:
                    self.direction = "right"
                    self.xchange = 5
                    if self.rect.y == 550-self.scaledheight:
                        self.animation = "run"

                else:
                    self.animation = "idle"
                   
                if self.jumping:
                    if self.ychange < 0:
                        self.animation = "jump"
                    else:
                        self.animation = "fall"
                    if self.rect.y >= 250 and self.ychange > 0:
                        self.jumping = False
                        self.busy = False
            else:
                
                self.busy = True
                self.kicking = False
                self.zapping = False
                self.jumping = False
                self.count += 1
                if self.count > 35:
                    if self.hpremoved == False:
                        self.hp -= 1
                        self.hpremoved = True                        
                    self.animation = "stun"
                    if self.dist(other) > 0:
                        self.xchange = 5
                    else:
                        self.xchange = -5
                if self.count > 45:
                    
                    self.count = 0
                    self.stunned = False
                    self.busy = False
            
            if not self.moveright and not self.moveleft and not self.stunned:
                self.xchange = 0
            self.gravity()   
         
        self.rect.x += self.xchange
        self.rect.y += self.ychange                
                    
        #animeerimise kood
        if self.frame_count >= len(self.sprites[self.animation])*10-1:
            self.frame_count = 0
        else:
            self.frame_count += 1
        if not self.jumping:
            self.animframe = self.frame_count//10


   
        
    def gravity(self):
        self.ychange += .35
        # kontroll kas tegelane on maa peal või õhus
        if self.rect.y >= 550-self.scaledheight and self.ychange >= 0:
            self.ychange = 0
            self.rect.y = 550-self.scaledheight
            self.jumps = 1
    # hüppamine
    def jump(self):
        if not self.busy:
            if self.jumps == 1:
                self.busy = True
                self.reset()
                self.ychange = -12
                self.jumping = True
                self.jumps -= 1
        
    def move(self, direction):
        if not self.busy or self.jumping:
            if direction == "left":
                self.moveleft = True
                self.moveright = False
            else:
                self.moveright = True
                self.moveleft = False
        
    def dist(self, other):
        return self.rect.x-other.rect.x
        
    # kaks erinevat lööki, kiire ja lühikese ulatusega ning aeglane ja pika ulatusega        
    def slow(self, other):
        if self.busy == False:
            self.moveright = False
            self.moveleft = False
            self.busy = True
            global first
            first = self.spritesheet
            self.reset()
            if self.zapping == False:
                self.zapping = True
                if self.direction == "right":
                    if self.dist(other) <= -35 and self.dist(other) >= -400:
                        other.hpremoved = False
                        other.stunned = True
                else:
                    if self.dist(other) >= 35 and self.dist(other) <= 400:
                        other.hpremoved = False
                        other.stunned = True


    def fast(self, other):
        if not self.busy:
            self.moveright = False
            self.moveleft = False
            self.busy = True
            global first
            first = self.spritesheet
            self.reset()
            if self.kicking == False:
                self.kicking = True
                if self.direction == "right":
                    if self.dist(other) <= -35 and self.dist(other) >= -145:
                        other.hpremoved = False
                        other.stunned = True
                else:
                    if self.dist(other) >= 35 and self.dist(other) <= 145:
                        other.hpremoved = False
                        other.stunned = True
    
        
    def reset(self):
        self.animframe = 0
        self.frame_count = 0

 
pygame.init() 
window=[1000,600] 
screen=pygame.display.set_mode(window) 
pygame.display.set_caption("Kaklus") 

backgroundframes = []

for i in range(8):
    backgroundframes.append(pygame.image.load("taust/frame_00" + str(i) + ".gif").convert())

# loon 2 tegelast kasutades Player klassi
player1 = Player(900-45*3,285,"surge3.png", "left")
player2 = Player(100,285,"black3.png", "right")


done = False
clock = pygame.time.Clock() 
frame = 0
background = backgroundframes[0]
backgroundx = 0
first = "surge3.png"

# main loop
while not done: 
     
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
 
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                player1.move("left")
            if event.key == pygame.K_RIGHT:
                player1.move("right")
            if event.key == pygame.K_UP:
                player1.jump()
            if event.key == pygame.K_KP0:
                player1.fast(player2)
            if event.key == pygame.K_KP_PERIOD:
                player1.slow(player2)
            if event.key == pygame.K_ESCAPE:
                done = True

        if event.type == pygame.KEYUP: 
            if event.key == pygame.K_LEFT: 
                player1.moveleft = False
            if event.key == pygame.K_RIGHT: 
                player1.moveright = False
                
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                player2.move("left")
            if event.key == pygame.K_d:
                player2.move("right")
            if event.key == pygame.K_w:
                player2.jump()
            if event.key == pygame.K_k:
                player2.fast(player1)
            if event.key == pygame.K_l:
                player2.slow(player1)
                 
        if event.type == pygame.KEYUP: 
            if event.key == pygame.K_a: 
                player2.moveleft = False
            if event.key == pygame.K_d: 
                player2.moveright = False
                
 


    
    if frame >= 47:
        frame = 0
    else:
        frame += 1
    background = backgroundframes[frame//6]

    screen.blit(background, (0,0), (backgroundx,0,1000,600))
    
    
    if first == "surge3.png":
        player1.draw(screen, player2)
        player2.draw(screen, player1)
    else:
        player2.draw(screen, player1)
        player1.draw(screen, player2)
        
    pygame.display.flip() 
    clock.tick(120) 

pygame.quit ()
