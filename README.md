Kaklus
======

### What is this?

A simple 2d fighting game written in python 3 during my first semester in uni.
The code is very ugly and the game is extremely basic. There are also some bugs, but I'm not going to change anything, just keeping this for when I'm feeling nostalgic.

(Requires Python3 and PyGame)

### How do I start playing?

Download the game (including the graphical assets) [here](https://bitbucket.org/LQKI/kaklus/downloads/kaklus.zip) or [here](https://github.com/LQKI/kaklus/releases/download/v1.0/kaklus.zip). 
Unzip and run kaklus.py using python3.

## Controls

Black moves with WASD, hits with K and L
Orange moves with the arrow keys, hits with numpad 0 and numpad .
Escape key to exit.

## Game assets

Character sprites taken from: http://opengameart.org/content/surge-of-opensurge-for-ultimate-smash-friends (http://opensnc.sourceforge.net/home/index.php)

Background image taken from this gallery: http://imgur.com/gallery/GPlx4

